import React from 'react';
import BasicLayout from './components/basic-layout';
import './App.css'

function App() {
  return (
    <div className="App">
      <BasicLayout />
    </div>
  );
}

export default App;
